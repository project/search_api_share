This module allows to use single SOLR core from many sites.

Define an index name in settings.php, like:

$conf['search_api_share_index_prefix'] = 'name';
