<?php
/**
 * @file
 * Search API share functions.
 */

/**
 * Implements hook_search_api_service_info().
 */
function search_api_share_search_api_service_info() {
  return array(
    'search_api_share_service' => array(
      'name' => t('Search API share'),
      'description' => t('<p>Index items using the Search API share SOLR Search service.<p>'),
      'class' => 'SearchApiShareSolrService',
    ),
    'search_api_share_acquia_search_service' => array(
      'name' => t('Search API share in Acquia'),
      'description' => t('<p>Index items using the Search API share Acquia Search service.<p>'),
      'class' => 'SearchApiShareAcquiaSearchService',
    ),
  );
}

/**
 * Alter default search indexes.
 *
 * @param array $defaults
 *   An array of default search indexes, keyed by machine names.
 *
 * @see hook_default_search_api_index()
 */
function search_api_share_default_search_api_index_alter(array &$defaults) {
  // Changes default node index's server.
  $default_index = variable_get('search_api_share_shared_index', '');
  if ($server_name = variable_get('search_api_share_search_api_server_name', '')) {
    if (isset($defaults[$default_index]->server)) {
      $defaults[$default_index]->server = $server_name;
    }
  }
}

/**
 * Helper function to get index name.
 */
function search_api_share_node_index_name($shared_index) {
  return variable_get('search_api_share_index_prefix', 'default_node_index') . '_' . $shared_index;
}

/**
 * Implements hook_search_api_solr_documents_alter().
 */
function search_api_share_search_api_solr_documents_alter(&$documents, $index, $items) {
  $shared_index = variable_get('search_api_share_shared_index');
  if ($index->machine_name == $shared_index) {
    foreach ($documents as &$document) {
      $item_id = $document->getField('item_id');
      $document->setField('index_id', search_api_share_node_index_name($shared_index));
      $document->setField('id', search_api_share_node_index_name($shared_index) . '-' . $item_id['value']);
    }
  }
}

/**
 * Implements hook_search_api_solr_query_alter().
 */
function search_api_share_search_api_solr_query_alter(array &$call_args, SearchApiQueryInterface $query) {
  $shared_index = variable_get('search_api_share_shared_index');
  foreach ($call_args['params']['fq'] as &$param) {
    if ($param == "index_id:$shared_index") {
      $param = 'index_id:' . search_api_share_node_index_name($shared_index);
    }
  }
}
